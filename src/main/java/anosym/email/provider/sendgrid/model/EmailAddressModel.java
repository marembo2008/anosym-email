package anosym.email.provider.sendgrid.model;

import javax.annotation.Nonnull;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 10:22:42 PM
 */
@Data
@Builder
public class EmailAddressModel {

  @Nonnull
  private final String email;

  @Nonnull
  private final String name;

}
