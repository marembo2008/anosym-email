package anosym.email.provider.sendgrid.model;

import java.util.List;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 10:43:33 PM
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AddressPersonalizationModel {

  @Nonnull
  @Singular
  @JsonProperty("to")
  private final List<EmailAddressModel> toAddresses;

  @Nonnull
  @Singular
  @JsonProperty("cc")
  private final List<EmailAddressModel> ccAddresses;

  @Nonnull
  @Singular
  @JsonProperty("bcc")
  private final List<EmailAddressModel> bccAddresses;

}
