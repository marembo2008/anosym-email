package anosym.email.provider.sendgrid.model;

import javax.annotation.Nonnull;

import lombok.Builder;
import lombok.Data;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 10:53:27 PM
 */
@Data
@Builder
public class EmailContentModel {

  @Nonnull
  private final String type;

  @Nonnull
  private final String value;

}
