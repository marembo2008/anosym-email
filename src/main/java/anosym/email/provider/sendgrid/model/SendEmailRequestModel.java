package anosym.email.provider.sendgrid.model;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 10:22:08 PM
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SendEmailRequestModel {

  @Nonnull
  @Singular
  @JsonProperty("personalizations")
  private final List<AddressPersonalizationModel> addresses;

  @Nonnull
  @JsonProperty("from")
  private final EmailAddressModel fromAddress;

  @Nonnull
  @JsonProperty("reply_to")
  private final EmailAddressModel replyToAddress;

  @Nonnull
  private final String subject;

  @Nullable
  @JsonProperty("template_id")
  private final String templateId;

  @Nullable
  @JsonProperty("content")
  private final List<EmailContentModel> contents;

}
