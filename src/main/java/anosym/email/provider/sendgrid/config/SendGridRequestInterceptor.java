package anosym.email.provider.sendgrid.config;

import static java.lang.String.format;

import javax.annotation.Nonnull;

import anosym.feign.FeignClientId;
import anosym.feign.interceptor.ApiRequestInterceptor;
import anosym.feign.interceptor.FeignInterceptor;
import feign.RequestTemplate;
import jakarta.enterprise.inject.spi.CDI;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 11:37:38 PM
 */
@FeignInterceptor("sendgrid")
public class SendGridRequestInterceptor implements ApiRequestInterceptor {

  @Override
  public void apply(@Nonnull final FeignClientId feignClientId, @Nonnull final RequestTemplate requestTemplate) {
    final String apiKey = CDI.current()
            .select(SendGridEmailConfigurationService.class)
            .get()
            .getApiKey();
    requestTemplate.header("Authorization", format("Bearer %s", apiKey));
  }

}
