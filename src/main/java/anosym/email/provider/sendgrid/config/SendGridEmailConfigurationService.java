package anosym.email.provider.sendgrid.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Default;
import khameleon.core.annotations.Encrypt;
import khameleon.core.annotations.Info;
import khameleon.core.annotations.Password;

/**
 *
 * @author marembo
 */
public interface SendGridEmailConfigurationService {

  @Nonnull
  @Info("The source email address, where emails are sent from")
  String getFromAddress();

  @Nonnull
  @Info("The named sender, where emails are sent from")
  String getFromName();

  @Nonnull
  @Encrypt
  @Password
  @Info("The SendGrid api key used to send email.")
  String getApiKey();

  @Nonnull
  @Info("The SendGrid api endpoint.")
  @Default("https://api.sendgrid.com")
  String getApiEndpoint();

}
