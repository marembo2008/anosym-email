package anosym.email.provider.sendgrid.config;

import javax.annotation.Nonnull;

import anosym.feign.config.FeignConfig;
import anosym.feign.config.FeignConfigProvider;
import jakarta.enterprise.inject.spi.CDI;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 11:31:25 PM
 */
@FeignConfig("sendgrid")
public class SendGridFeignConfigProvider implements FeignConfigProvider {

  @Override
  public String getEndPoint(@Nonnull final String feignClientInstanceId) {
    return CDI.current()
            .select(SendGridEmailConfigurationService.class)
            .get()
            .getApiEndpoint();
  }

}
