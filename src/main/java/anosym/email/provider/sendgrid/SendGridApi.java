package anosym.email.provider.sendgrid;

import javax.annotation.Nonnull;

import anosym.email.provider.sendgrid.model.SendEmailRequestModel;
import anosym.feign.FeignClient;
import feign.Headers;
import feign.RequestLine;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 24, 2019, 10:19:22 PM
 */
@FeignClient("sendgrid")
public interface SendGridApi {

  @RequestLine("POST /v3/mail/send")
  @Headers({"Content-Type: application/json", "Accept: application/json"})
  void sendMail(@Nonnull final SendEmailRequestModel sendEmailRequest);

}
