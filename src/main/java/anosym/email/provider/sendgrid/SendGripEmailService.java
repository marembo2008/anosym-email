package anosym.email.provider.sendgrid;

import javax.annotation.Nonnull;

import anosym.email.MailCallback;
import anosym.email.MailMessage;
import anosym.email.provider.EmailService;
import anosym.email.provider.sendgrid.config.SendGridEmailConfigurationService;
import anosym.email.provider.sendgrid.model.AddressPersonalizationModel;
import anosym.email.provider.sendgrid.model.EmailAddressModel;
import anosym.email.provider.sendgrid.model.EmailContentModel;
import anosym.email.provider.sendgrid.model.SendEmailRequestModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static java.util.Arrays.asList;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 6:21:36 AM
 */
@Slf4j
@RequiredArgsConstructor
public class SendGripEmailService implements EmailService {

  @Nonnull
  private final SendGridEmailConfigurationService emailConfigurationService;

  @Nonnull
  private final SendGridApi sendGridApi;

  @Override
  public void sendHtmlMail(@Nonnull final String subject,
                           @Nonnull final String messageBody,
                           @Nonnull final String recipient) {
    log.info("Sending email to <{}>, subject <{}>:", recipient, subject, messageBody);

    final EmailAddressModel fromAndReplyToAddress = EmailAddressModel.builder()
            .email(emailConfigurationService.getFromAddress())
            .name(emailConfigurationService.getFromName())
            .build();
    final EmailAddressModel toAddress = EmailAddressModel.builder()
            .email(recipient)
            .name(recipient)
            .build();
    final AddressPersonalizationModel addressPersonalization = AddressPersonalizationModel.builder()
            .toAddress(toAddress)
            .build();
    final EmailContentModel content = EmailContentModel.builder()
            .type("text/html")
            .value(messageBody)
            .build();
    final SendEmailRequestModel request = SendEmailRequestModel.builder()
            .address(addressPersonalization)
            .contents(asList(content))
            .subject(subject)
            .fromAddress(fromAndReplyToAddress)
            .replyToAddress(fromAndReplyToAddress)
            .build();
    sendGridApi.sendMail(request);
  }

  @Override
  public void sendHtmlMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void sendMail(@Nonnull final String subject, @Nonnull final String messageBody, @Nonnull final String recipient) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void sendMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

}
