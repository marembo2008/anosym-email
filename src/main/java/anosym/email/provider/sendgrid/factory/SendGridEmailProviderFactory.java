package anosym.email.provider.sendgrid.factory;

import static anosym.email.provider.config.EmailProvider.SEND_GRID;

import anosym.email.provider.EmailService;
import anosym.email.provider.factory.EmailProviderFactory;
import anosym.email.provider.factory.ForEmailProvider;
import anosym.email.provider.sendgrid.SendGridApi;
import anosym.email.provider.sendgrid.SendGripEmailService;
import anosym.email.provider.sendgrid.config.SendGridEmailConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 6:25:48 AM
 */
@ApplicationScoped
@ForEmailProvider(SEND_GRID)
class SendGridEmailProviderFactory implements EmailProviderFactory {

  @Inject
  private SendGridEmailConfigurationService emailConfigurationService;

  @Inject
  private SendGridApi sendGridApi;

  @Override
  public EmailService getEmailService() {
    return new SendGripEmailService(emailConfigurationService, sendGridApi);
  }

}
