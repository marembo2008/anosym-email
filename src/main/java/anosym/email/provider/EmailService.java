package anosym.email.provider;

import javax.annotation.Nonnull;

import anosym.email.MailCallback;
import anosym.email.MailMessage;

/**
 *
 * @author marembo
 */
public interface EmailService {

    void sendMail(@Nonnull final String subject, @Nonnull final String messageBody, @Nonnull final String recipient);

    void sendHtmlMail(@Nonnull final String subject, @Nonnull final String messageBody, @Nonnull final String recipient);

    /**
     * Sends mail asynchronously, returning immediately and the callback notified of status.
     */
    void sendMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback);

    /**
     * Sends mail asynchronously, returning immediately and the callback notified of status.
     */
    void sendHtmlMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback);
}
