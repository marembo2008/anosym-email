package anosym.email.provider.config;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 5:41:22 AM
 */
public enum EmailProvider {

  SMTP,
  SEND_GRID

}
