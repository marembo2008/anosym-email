package anosym.email.provider.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Default;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 5:46:25 AM
 */
public interface EmailProviderConfigurationService {

  @Nonnull
  @Default("SEND_GRID")
  EmailProvider getEnabledEmailProvider();

}
