package anosym.email.provider.factory;

import javax.annotation.Nonnull;

import anosym.email.provider.EmailService;
import anosym.email.provider.config.EmailProvider;
import anosym.email.provider.config.EmailProviderConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.enterprise.inject.Any;
import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 6:22:18 AM
 */
@ApplicationScoped
public class EmailProviderProducer {

  @Inject
  private EmailProviderConfigurationService emailProviderConfigurationService;

  @Any
  @Inject
  private Instance<EmailProviderFactory> emailProviderFactories;

  @Produces
  @Dependent
  public EmailService createEmailService(@Nonnull final InjectionPoint ip) {
    final ForEmailProvider forEmailProvider = getForEmailProvider(ip);
    return emailProviderFactories
            .select(forEmailProvider)
            .get()
            .getEmailService();
  }

  @Nonnull
  private ForEmailProvider getForEmailProvider(@Nonnull final InjectionPoint ip) {
    final ForEmailProvider forEmailProvider = ip.getAnnotated().getAnnotation(ForEmailProvider.class);
    if (forEmailProvider != null) {
      return forEmailProvider;
    }

    final EmailProvider emailProvider = emailProviderConfigurationService.getEnabledEmailProvider();
    return new ForEmailProvider.ForEmailProviderLiteral(emailProvider);
  }

}
