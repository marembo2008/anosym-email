package anosym.email.provider.factory;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.annotation.Nonnull;

import anosym.email.provider.config.EmailProvider;
import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.inject.Qualifier;
import lombok.AllArgsConstructor;

import static anosym.email.provider.config.EmailProvider.SMTP;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 5:41:54 AM
 */
@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE, FIELD, METHOD, PARAMETER})
public @interface ForEmailProvider {

  EmailProvider value() default SMTP;

  @AllArgsConstructor
  class ForEmailProviderLiteral extends AnnotationLiteral<ForEmailProvider> implements ForEmailProvider {

    @Nonnull
    private EmailProvider emailProvider;

    @Override
    public EmailProvider value() {
      return emailProvider;
    }

  }

}
