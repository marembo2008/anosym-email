package anosym.email.provider.factory;

import javax.annotation.Nonnull;

import anosym.email.provider.EmailService;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 6:24:56 AM
 */
public interface EmailProviderFactory {

  @Nonnull
  EmailService getEmailService();

}
