package anosym.email.provider.smtp.config;

import javax.annotation.Nonnull;

import khameleon.core.annotations.Encrypt;
import khameleon.core.annotations.Info;
import khameleon.core.annotations.Password;

/**
 *
 * @author marembo
 */
public interface SmtpEmailConfigurationService {

  @Nonnull
  @Info("The source email address, where emails are sent from")
  String getSourceAddress();

  @Nonnull
  @Encrypt
  @Password
  @Info("The password, of the source email address, from which emails are sent from.")
  String getPassword();

  @Nonnull
  @Info("The smtp server, e.g. smtp.gmail.com")
  String getServer();

  @Info("The smtp server port.")
  int getPort();

  @Info("If authentication is enabled for the source email address.")
  boolean isEnableAuthentication();

  @Info("If tls is enabled for sending the emails.")
  boolean isEnableTls();

}
