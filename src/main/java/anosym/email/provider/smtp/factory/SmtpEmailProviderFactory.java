package anosym.email.provider.smtp.factory;

import static anosym.email.provider.config.EmailProvider.SMTP;

import anosym.email.provider.EmailService;
import anosym.email.provider.factory.EmailProviderFactory;
import anosym.email.provider.factory.ForEmailProvider;
import anosym.email.provider.smtp.SmtpEmailService;
import anosym.email.provider.smtp.config.SmtpEmailConfigurationService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

/**
 *
 * @author marembo (marembo2008@gmail.com)
 * @since Jun 18, 2019, 6:25:48 AM
 */
@ApplicationScoped
@ForEmailProvider(SMTP)
class SmtpEmailProviderFactory implements EmailProviderFactory {

  @Inject
  private SmtpEmailConfigurationService smtpEmailConfigurationService;

  @Override
  public EmailService getEmailService() {
    return new SmtpEmailService(smtpEmailConfigurationService);
  }

}
