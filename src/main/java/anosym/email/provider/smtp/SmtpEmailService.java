package anosym.email.provider.smtp;

import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.Properties;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import anosym.async.interceptor.Asynchronous;
import anosym.email.MailCallback;
import anosym.email.MailMessage;
import anosym.email.provider.EmailService;
import anosym.email.provider.smtp.config.SmtpEmailConfigurationService;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.activation.FileDataSource;
import jakarta.mail.Authenticator;
import jakarta.mail.Message;
import jakarta.mail.Multipart;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author marembo
 */
@Slf4j
public class SmtpEmailService implements EmailService {

  private static final String DEFAULT_CONTENT_TYPE = "text/plain";

  private final SmtpEmailConfigurationService emailConfigurationService;

  /**
   * @param emailConfigurationService
   */
  public SmtpEmailService(@Nonnull final SmtpEmailConfigurationService emailConfigurationService) {
    this.emailConfigurationService = checkNotNull(emailConfigurationService,
                                                  "The emailConfigurationService must not be null");
  }

  @Override
  public void sendMail(@Nonnull final String subject,
                       @Nonnull final String messageBody,
                       @Nonnull final String recipient) {
    checkNotNull(subject, "The subject must not be null");
    checkNotNull(messageBody, "The messageBody must not be null");
    checkNotNull(recipient, "The recipient must not be null");

    doSendMail(subject, messageBody, recipient, DEFAULT_CONTENT_TYPE);
  }

  @Override
  public void sendHtmlMail(@Nonnull final String subject,
                           @Nonnull final String messageBody,
                           @Nonnull final String recipient) {
    checkNotNull(subject, "The subject must not be null");
    checkNotNull(messageBody, "The messageBody must not be null");
    checkNotNull(recipient, "The recipient must not be null");

    doSendMail(subject, messageBody, recipient, "text/html");
  }

  @Override
  @Asynchronous
  public void sendMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback) {
    checkNotNull(mailMessage, "The mailMessage must not be null");
    checkNotNull(mailCallback, "The mailCallback must not be null");

    final Properties properties = getProperties();
    final Session session = Session.getInstance(properties, getAuthenticator());
    doSendMail(mailMessage, session, DEFAULT_CONTENT_TYPE, mailCallback);
  }

  @Override
  @Asynchronous
  public void sendHtmlMail(@Nonnull final MailMessage mailMessage, @Nonnull final MailCallback mailCallback) {
    checkNotNull(mailMessage, "The mailMessage must not be null");
    checkNotNull(mailCallback, "The mailCallback must not be null");

    final Properties properties = getProperties();
    final Session session = Session.getInstance(properties, getAuthenticator());
    doSendMail(mailMessage, session, "text/html", mailCallback);
  }

  @SneakyThrows
  private void doSendMail(@Nonnull final String subject,
                          @Nonnull final String body,
                          @Nonnull final String recipient,
                          @Nonnull final String contentType) {
    final Properties properties = getProperties();
    final Session session = Session.getInstance(properties, getAuthenticator());
    final String sourceAddress = emailConfigurationService.getSourceAddress();
    final Message message = new MimeMessage(session);
    message.setFrom(new InternetAddress(sourceAddress));
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
    message.setSubject(subject);
    message.setContent(body, firstNonNull(contentType, DEFAULT_CONTENT_TYPE));
    Transport.send(message);

    log.info("Sent Message: {0}", message);
  }

  private void doSendMail(@Nonnull final MailMessage mailMessage,
                          @Nonnull final Session session,
                          @Nullable final String contentType,
                          @Nonnull final MailCallback mailCallback) {
    try {
      final String sourceAddress = emailConfigurationService.getSourceAddress();
      final Message message = new MimeMessage(session);
      message.setFrom(new InternetAddress(sourceAddress));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailMessage.getToAddress()));

      if (mailMessage.getCcAddress() != null) {
        for (final String cc : mailMessage.getCcAddress()) {
          message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
        }
      }
      if (mailMessage.getBccAddress() != null) {
        for (final String bcc : mailMessage.getBccAddress()) {
          message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
        }
      }
      message.setSubject(mailMessage.getSubject());

      //body part
      final MimeBodyPart txtMessage = new MimeBodyPart();
      txtMessage.setContent(mailMessage.getMessage(), firstNonNull(contentType, DEFAULT_CONTENT_TYPE));

      //create multipart message
      final Multipart mp = new MimeMultipart();
      mp.addBodyPart(txtMessage);
      if (mailMessage.getAttachments() != null) {
        for (final String file : mailMessage.getAttachments()) {
          final MimeBodyPart attach = new MimeBodyPart();
          final DataSource attachsource = new FileDataSource(file);
          attach.setDataHandler(new DataHandler(attachsource));
          attach.setFileName(new File(file).getName());
          mp.addBodyPart(attach);
        }
      }
      message.setContent(mp);
      Transport.send(message);
      mailCallback.onSent();
    } catch (final Exception e) {
      log.error("Failed to send mail", e);
      mailCallback.onFailure();
    }
  }

  @Nonnull
  private Properties getProperties() {
    final Properties props = new Properties();
    props.put("mail.smtp.auth", emailConfigurationService.isEnableAuthentication());
    props.put("mail.smtp.starttls.enable", emailConfigurationService.isEnableTls());
    props.put("mail.smtp.host", emailConfigurationService.getServer());
    props.put("mail.smtp.port", emailConfigurationService.getPort());
    return props;
  }

  @Nonnull
  private Authenticator getAuthenticator() {
    final String sourceAddress = emailConfigurationService.getSourceAddress();
    final String password = emailConfigurationService.getPassword();
    final PasswordAuthentication passwordAuthentication = new PasswordAuthentication(sourceAddress, password);

    return new Authenticator() {

      @Override
      @Nonnull
      protected PasswordAuthentication getPasswordAuthentication() {
        return passwordAuthentication;
      }

    };
  }

}
