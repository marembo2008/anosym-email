package anosym.email;

/**
 *
 * @author marembo
 */
public interface MailCallback {

    void onSent();

    void onFailure();
}
